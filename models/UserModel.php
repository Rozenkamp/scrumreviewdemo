<?php

class UserModel
{
    protected $conn;
    public function __construct($conn)
    {
        $this->conn = $conn;

    }

    public function allUsers()
    {
        $sql = "SELECT * FROM users";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}