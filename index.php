<?php
//require
require 'database/Connection.php';
require 'models/UserModel.php';

//Connection class
$conn = Connection::make();

$data = new UserModel($conn);
$users = $data->allUsers();
//view

require 'views/index.view.php';